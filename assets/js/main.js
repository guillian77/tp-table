/**
 * -----------------------------------------------------
 * DECLARE SOME DATA
 * -----------------------------------------------------
 */
let aOfPersonnes= [];

aOfPersonnes[0]= [];
aOfPersonnes[0]["prenom"]= "Natasha";
aOfPersonnes[0]["nom"]= "Romanoff";
aOfPersonnes[0]["age"]= "32";
aOfPersonnes[0]["tel"]= "0601020304";
aOfPersonnes[0]["mail"]= "natasha@marvel.universe";

aOfPersonnes[1]= [];
aOfPersonnes[1]["prenom"]= "Tony";
aOfPersonnes[1]["nom"]= "Stark";
aOfPersonnes[1]["age"]= "47";
aOfPersonnes[1]["tel"]= "0706050403";
aOfPersonnes[1]["mail"]= "tony@marvel.universe";

aOfPersonnes[2]= [];
aOfPersonnes[2]["prenom"]= "Steve";
aOfPersonnes[2]["nom"]= "Rogers";
aOfPersonnes[2]["age"]= "9999";
aOfPersonnes[2]["tel"]= "0706050403";
aOfPersonnes[2]["mail"]= "steve@marvel.universe";

aOfPersonnes[3]= [];
aOfPersonnes[3]["prenom"]= "Carol";
aOfPersonnes[3]["nom"]= "Danvers";
aOfPersonnes[3]["age"]= "36";
aOfPersonnes[3]["tel"]= "0706050404";
aOfPersonnes[3]["mail"]= "carol@marvel.universe";

aOfPersonnes[4]= [];
aOfPersonnes[4]["prenom"]= "Bruce";
aOfPersonnes[4]["nom"]= "Banner";
aOfPersonnes[4]["age"]= "52";
aOfPersonnes[4]["tel"]= "0606050803";
aOfPersonnes[4]["mail"]= "bruce@marvel.universe";

/**
 * -----------------------------------------------------
 * CONSTRUCT TABLE
 * -----------------------------------------------------
 */
function constructTable()	{
    var i;

    var sHTML= "";
    sHTML+= '<thead class="thead-dark">';
    sHTML+= "<tr>";
    sHTML+=     "<th>Prénom</th>";
    sHTML+=     "<th>Nom</th>";
    sHTML+=     "<th>Email</th>";
    sHTML+=     "<th>Téléphone</th>";
    sHTML+=     "<th>Éditer</th>";
    sHTML+=     "<th>Supprimer</th>";
    sHTML+= "</tr>";
    sHTML+= "</thead>";
    sHTML+= "<tbody>";

    for (let i = 0; i < aOfPersonnes.length; i++)
    {
        sHTML+= "<tr>";
        sHTML+=     "<td>" + aOfPersonnes[i]["prenom"] + "</td>";
        sHTML+=     "<td>" + aOfPersonnes[i]["nom"] + "</td>";
        sHTML+=     "<td>" + aOfPersonnes[i]["mail"] + "</td>";
        sHTML+=     "<td>" + aOfPersonnes[i]["tel"] + "</td>";
        sHTML+=     '<td onClick="editPersonne(' + i + ')" class="actionCell edit" title="Éditer les infos de ' + aOfPersonnes[i]["prenom"] + '"><i class="fas fa-edit"></i></td>';
        sHTML+=     '<td onClick="supprimPersonne(' + i + ')" class="actionCell delete" title="Supprimer les infos de ' + aOfPersonnes[i]["prenom"] + '"><i class="fas fa-user-minus"></i></td>';
        sHTML+= "</tr>";
    }
    
    sHTML+= "</tbody>";
    $('#avengers__table').html(sHTML);
}

/**
 * -----------------------------------------------------
 * COMMONS FUNCTIONS
 * -----------------------------------------------------
 */

/**
 * Clear fields
 */
function clearFields() {
    $('.form-control').each(function(input) {
        $(this).val("");
    });
}

/**
 * Display fields
 * @param {String} action Add, edit
 * @param {Boolean} clear Clear fields before displaying.
 */
function displayFields(clear = true, actionType) {
    if (clear) {
        clearFields(); // Clear fields before displaying.            
    }

    $(".fields").addClass('show');

    // If action type is to "Add" new avenger
    if (actionType === "add") {
        // Display cancel button
        $("#btn_annuler").addClass('show');

        // Switch add button to save button
        $("#btn_ajouter").html('Enregistrer cet Avenger');
        
    }
    else {
        $("#btn_ajouter").removeClass('show');
        $("#btn_modifier").addClass('show');
        $("#btn_supprimer").addClass('show');
        $("#btn_annuler").addClass('show');
    }
}

/**
 * Toggle Fields:
 * Display/ Hide fields.
 * @param {Boolean} clear Clear fields before displaying.
 * @return {Boolean} True if fields is visible when called.
 */
function toggleFields(clear = true, actionType)
{
    if ($(".fields").hasClass('show'))
    {
        console.log('coucou')

        // Hide all others buttons
        $(".fields").removeClass("show");
        $("#btn_annuler").removeClass("show");
        $('#btn_modifier').removeClass('show');
        $("#btn_supprimer").removeClass('show');

        // Back add button to default
        $("#btn_ajouter")
            .addClass('show')
            .html('<i class="fas fa-user-plus"></i> Ajouter un avenger');

        return true;
    }
    else
    {
        if (clear) {
            displayFields(true, actionType);
        } else {
            displayFields(false, actionType);
        }

        return false;
    }
}

/**
 * Rebuild table
 * Prevent errors from Datatable object after table update.
 * Without this function table's elements can't be sorted anymore.
 */
function rebuildTable() {
    $('#avengers__table').html("");
    tables.clear(); 
    tables.destroy(); 
    constructTable();
    tables = $('#avengers__table').DataTable(configuration);
}

/**
 * -----------------------------------------------------
 * CRUD
 * -----------------------------------------------------
 */

/**
 * Add
 */
function ajoutPersonne() {
    if (toggleFields(true, "add")) {
        var iLongueur= aOfPersonnes.length;
        aOfPersonnes[iLongueur]= [];
        aOfPersonnes[iLongueur]["prenom"]= $('#prenom').val();
        aOfPersonnes[iLongueur]["nom"]= $('#nom').val();
        aOfPersonnes[iLongueur]["age"]= $('#age').val();
        aOfPersonnes[iLongueur]["tel"]= $('#telephone').val();
        aOfPersonnes[iLongueur]["mail"]= $('#email').val();
        rebuildTable();
    }
}

/**
 * Edit
 */
var iIndiceEditionEncours;
function editPersonne(iIndiceEdit)	{
    iIndiceEditionEncours= iIndiceEdit;

    $('#prenom').val( aOfPersonnes[iIndiceEdit]["prenom"] );
    $('#nom').val( aOfPersonnes[iIndiceEdit]["nom"] );
    $('#age').val( aOfPersonnes[iIndiceEdit]["age"] );
    $('#telephone').val( aOfPersonnes[iIndiceEdit]["tel"] );
    $('#email').val( aOfPersonnes[iIndiceEdit]["mail"] );

    $('#btn_supprimer').attr('onclick', 'supprimPersonne(' + iIndiceEdit + ', true)')

    displayFields(false, "edit");
}

/**
 * Update
 */
function majPersonne() {
    // Edit avenger with fields
    aOfPersonnes[iIndiceEditionEncours]['prenom'] = $('#prenom').val();
    aOfPersonnes[iIndiceEditionEncours]['nom'] = $('#nom').val();
    aOfPersonnes[iIndiceEditionEncours]['age'] = $('#age').val();
    aOfPersonnes[iIndiceEditionEncours]['tel'] = $('#telephone').val();
    aOfPersonnes[iIndiceEditionEncours]['mail'] = $('#email').val();

    // Toggle fields & clear fields
    toggleFields(true);

    rebuildTable();
}

/**
 * Delete
 * @param {Number} index Index of avenger to delete.
 * @param {Boolean} showFields Display fields or not.
 */
function supprimPersonne(index, showFields = false) {
    // Delete avenger from array by index
    aOfPersonnes.splice(index, 1);

    // Toggle fields & clear fields
    if (showFields) { toggleFields(true); }
    
    rebuildTable();
}

var tables;
$(document).ready(function() {
    constructTable();
    tables = $('#avengers__table').DataTable(configuration);
});